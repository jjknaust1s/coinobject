const coin = {
    state: 0,
    flip: function() {
        this.state = Math.round(Math.random())
            // 1. One point: Randomly set your coin object's "state" property to be either 
            //    0 or 1: use "this.state" to access the "state" property on this object.
    },
    toString: function() {
        if (this.state === 0) {
            return 'heads'
        } else {
            return 'tails'
        }
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
    },
    toHTML: function() {
        const image = document.createElement('img')
        image.style.height = '50px'
        image.style.width = '50px'
        image.style.margin = '5px'
        if (this.state === 0) {
            image.setAttribute('src', 'images/Heads.jpg')

        } else {
            image.setAttribute('src', 'images/tails.jpg')
        }
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        return image;
    }
};

function display20Flips() {
    const results = [];
    for (let i = 0; i < 20; i++) {
        coin.flip()
        results.push(coin.toString())
        var flipResults = document.createElement('p')
        flipResults.innerHTML = results;
        console.log(results)
    }
    document.body.appendChild(flipResults)
        // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
}

function display20Images() {
    const results = [];
    for (let i = 0; i < 20; i++) {
        coin.flip()
        document.body.appendChild(coin.toHTML())
    }
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
}
display20Flips()
display20Images()